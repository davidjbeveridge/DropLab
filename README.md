# DropLab

A generic dropdown for all of your custom dropdown needs. 

## Install
* Add the JS to your JS directory.
* Add the CSS to your CSS directory. 
* Link to them in your code. See `index.html` for a sample.

## Usage
* Create a `ul` with `li`'s. This can serve static data or your dynamic data. Or both.
* If you are serving a static data just add it to the li's. 
```
  <div class="dropdown">
    <a href='#' data-dropdown-trigger='#dropdown-2'>
    <ul class="dropdown-menu" id="dropdown-2" data-dropdown>
      <li><a href="#">Some names</a></li>
    </ul>
  </div>
```

* If you are serving dynamic data use the `li` as a template. Your data will fill any custom fields you create.
* Mark the `ul` as `data-dynamic`, so it knows where to put the dynamic data. 
```
    <a href='#' data-id='main' data-dropdown-trigger='#dropdown-1'>
    <div class='dropdown-menu' id='dropdown-1' data-dropdown>
      <ul data-dynamic>
        <li><a href='#' data-id='{{id}}'>{{text}}</a></li>
      </ul>
    </div>
```
* Add some JS to set the data. Link it to the `data-id` of the trigger btn.

```
  <script>
    droplab.setData('main', [{
      id: 24601,
      text: 'Jacob'
    },
    {
      id: 24602,
      text: 'Jeff'
    }])
  </script>
```

* If you want to mix them up you can do that too.

```
<div class="container">
  <div class="dropdown">
    <a href='#' data-id='main' data-dropdown-trigger='#dropdown-1' class='btn'>Click me</a>
    <div class='dropdown-menu' id='dropdown-1' data-dropdown>
      <ul>
        <li><a href="#">About</a></li>
        <li><a href="#">Home</a></li>
      </ul>
      <ul data-dynamic>
        <li><a href='#' data-id='{{id}}'>{{text}}</a></li>
      </ul>
    </div>
  </div>
  <div class="dropdown">
    <input type='text' class='input' data-dropdown-trigger="#dropdown-2">
    <ul class="dropdown-menu" id="dropdown-2" data-dropdown>
      <li><a href="#" data-id='2403'>Some names</a></li>
    </ul>
  </div>
</div>
  <script src="droplab.js"></script>
  <script>
    droplab.setData('main', [{
      id: 24601,
      text: 'Jacob'
    },
    {
      id: 24602,
      text: 'Jeff'
    }])
  </script>
```

* You can also have an input as the trigger. Just use an `input` instead of a button. 
* When you type the dropdown will appear. 